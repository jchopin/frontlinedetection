% The script takes an image I, a background image I_bg, a threshold level
% as inputs. Image cleaning (using bwmorph) tuning may be necessary for 
% robust front detection. Outputs are col and row, the column and row 
% indices of detected points of the front.
%
% Before running the script check that : 
%   1- You work in the folder containing the script front_line_detection.
%   2- The folder with images and the script are in the same folder.

clear all;close all;clc;

% load image and background (for image substration)
dirname = ".\img\";
img_name = "PC_6-1400.tif";
filename = fullfile(dirname,img_name);
I = imread(filename); %load and pre-process image

img_name = "PC_6-1150.tif";
filename = fullfile(dirname,img_name);
I_bg = imread(filename); %load and pre-process image

%crop image
x1 = 320; y1 = 370; %upper left corner
x2 = 1800; y2 = 780-130; %bottom right corner
rect = [x1 y1 (x2-x1) abs(y2-y1)];
J = imcrop (I,rect);
J_bg = imcrop (I_bg,rect);

%background substraction
J = double(J)-double(J_bg);%imagesc(J)
N = size(J,2);

%Thresholding
level  = 5; % threshold level can be adjusted
K = J>level;

%Image cleaning
K=bwmorph(K,'fill');%imagesc(K);
K=~bwmorph(~K,'fill');%imagesc(K);
K=bwmorph(K,'close');%imagesc(K);

%Image gradient calculation along the vertical direction
dK = diff(K,1,1);%imagesc(dK);

%Find the points of the front (the 1s in the image)
col = zeros(1,N);
row = zeros(1,N);
counter = 0;
for j=1:N
    i = find(dK(:,j)==1,1,"first");
    if ~isempty(i)
        counter = counter + 1;
        col(counter) = j;
        row(counter) = i;
    end
end

% shift the coordinate to superpose detection front in the original image
col = col + x1;
row = row + y1;
if counter<N
    col(counter:end) = [];
    row(counter:end) = [];
end
imagesc(I);hold on;
plot(col,row,'r-');hold off;

